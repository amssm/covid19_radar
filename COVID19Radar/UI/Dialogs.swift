//
//  Dialogs.swift
//  COVID19Radar
//
//  Created by Ahmad Mahmoud on 22/12/2020.
//

import UIKit.UIAlertController
import UIKit.UIViewController

class Dialogs {
    
    private var alertController: UIAlertController?
    private weak var viewController: UIViewController?
    public typealias ToDoClosure = (_ title: String, _ details: String) -> Void
    
    func showDialog(viewController: UIViewController,
                    title: String = "message".localizedString(),
                    message: String,
                    okAction: @escaping () -> Void = {},
                    cancelAction: @escaping () -> Void = {}) {
        self.viewController = viewController
        alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController!.addAction(UIAlertAction(title: "ok".localizedString(), style: .default, handler: { _ in
            self.alertController?.dismiss(animated: true, completion: nil)
            okAction()
        }))
        alertController!.addAction(UIAlertAction(title: "cancel".localizedString(), style: .default, handler: { _ in
            self.alertController?.dismiss(animated: true, completion: nil)
            cancelAction()
        }))
        viewController.present(alertController!, animated: true, completion: nil)
    }
    
    func showLoading(viewController: UIViewController) {
        self.viewController = viewController
        self.alertController = UIAlertController(title: nil,
                                                 message: "loading".localizedString(),
                                                 preferredStyle: .alert)
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        if #available(iOS 13.0, *) { loadingIndicator.style = .medium }
        else { loadingIndicator.style = .gray }
        loadingIndicator.startAnimating()
        self.alertController!.view.addSubview(loadingIndicator)
        self.viewController?.present(alertController!, animated: true, completion: nil)
    }
    
    private func hideAletDialog() {
        self.alertController?.dismiss(animated: true, completion: nil)
        self.viewController = nil
    }
    
    func hideLoading() {
        self.hideDialog()
    }
    
    func hideDialog() {
        self.hideAletDialog()
    }
    
    func showActionSheet(viewController: UIViewController, title : String?,
                         message: String?,
                         actions : [UIAlertAction]) {
        self.viewController = viewController
        self.alertController = UIAlertController(title: title,
                                                 message: message,
                                                 preferredStyle: .actionSheet)
        if (!actions.isEmpty) {
            for action in actions {
                self.alertController!.addAction(action)
            }
        }
        self.viewController?.present(alertController!, animated: true, completion: nil)
    }
    
    func hideActionSheet() {
        self.hideDialog()
    }
}
