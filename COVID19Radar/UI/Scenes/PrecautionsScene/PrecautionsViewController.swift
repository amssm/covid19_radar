//
//  PrecautionsViewController.swift
//  COVID19Radar
//
//  Created by Ahmad Mahmoud on 25/12/2020.
//

import UIKit

class PrecautionsViewController: UIViewController {

    @IBOutlet private weak var generalPrecautionLabel: UILabel!
    @IBOutlet private weak var currentStatePrecautionsTextView: UITextView!
    //
    private var precautions: [String]!
    
    convenience init(precautions: [String]) {
        self.init()
        self.precautions = precautions
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "precautions".localizedString()
        self.navigationController?.navigationBar.topItem?.title = ""
        self.generalPrecautionLabel.text = "general_note".localizedString()
        self.currentStatePrecautionsTextView.isEditable = false
        self.currentStatePrecautionsTextView.attributedText = UIUtils.bulletPointsFrom(stringList: precautions)
    }
}
