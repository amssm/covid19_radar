//
//  StateSituationTrafficRepo.swift
//  COVID19Radar
//
//  Created by Ahmad Mahmoud on 23/12/2020.
//

import RxSwift

class StateSituationTrafficRepo {
    
    private let germanStatesAPI: Single<GermanStatesResponse>
    
    init(germanStatesAPI: Single<GermanStatesResponse>) {
        self.germanStatesAPI = germanStatesAPI
    }
    
    func getBavariaStateInfo() -> Single<StateInfo> {
        self.germanStatesAPI.flatMap({
            if let bavariaStateInfo = $0.states.first(where: { ($0.name == "Bayern") || ($0.code == "BY") }) {
                return Single.just(bavariaStateInfo)
            }
            else {
                let error = AppError(message: "bavaria_state_info_not_found".localizedString())
                return Single.error(error)
            }
        })
    }
}
