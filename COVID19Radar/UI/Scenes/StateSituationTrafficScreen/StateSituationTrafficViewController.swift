//
//  StateSituationTrafficViewController.swift
//  COVID19Radar
//
//  Created by Ahmad Mahmoud on 22/12/2020.
//

import UIKit

class StateSituationTrafficViewController: AppViewController<StateSituationTrafficPresenter, StateSituationTrafficView>, StateSituationTrafficView {

    @IBOutlet private weak var trafficLite: TrafficLight!
    @IBOutlet private weak var refreshButton: UIButton!
    @IBOutlet private weak var generalPrecautionLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        PushNotificationsUtils.shared.requestNotificationAuthorization { _ in
            let message = "push_notification_denied".localizedString()
            self.dialogs.showDialog(viewController: self, message: message)
        }
    }
    
    override func initUI() {
        self.title = "current_state".localizedString()
        self.generalPrecautionLabel.isHidden = true
        let generalNote = "general_note".localizedString() + "\n\n" + "click_traffic_lamp_for_more_info".localizedString()
        self.generalPrecautionLabel.text = generalNote
        self.refreshButton.layer.cornerRadius = 25
        self.refreshButton.backgroundColor = .blue
        self.refreshButton.isEnabled = false
        self.refreshButton.setTitleColor(.white, for: .normal)
        self.refreshButton.setTitle("refresh".localizedString(), for: .normal)
    }
    
    override func initPresenter() -> StateSituationTrafficPresenter {
        resolver.resolve(args: self)
    }
    
    override func showLoading() {
        self.trafficLite.startFlashingLights()
    }
    
    override func hideLoading() {
        self.trafficLite.stopFlasing()
    }
    
    override func didGetError(errorMessage: String) {
        super.didGetError(errorMessage: errorMessage)
        self.refreshButton.isEnabled = true
    }
    
    func didDenyLocationPermission() {
        self.refreshButton.isEnabled = true
        let title = "open_location_settings".localizedString()
        let message = "location_permission_message".localizedString()
        self.dialogs.showDialog(viewController: self, title: title, message: message, okAction:  {
            self.navigator.openLocationSettings()
        })
    }
    
    func didFailtToGetLocationCoordinates() {
        self.refreshButton.isEnabled = true
        self.dialogs.showDialog(viewController: self, message: "failed_to_get_location".localizedString())
    }
    
    func didGetCurrentState(state: State) {
        PushNotificationsUtils.shared.sendNotification()
        self.refreshButton.isEnabled = true
        self.generalPrecautionLabel.isHidden = false
        self.trafficLite.turnOnLamp(stateColor: state.color, clickAction: {
            self.navigator.openPrecautionsScreen(from: self, precautions: state.getPrecautions())
        })
    }
    
    @IBAction private func refreshButtonAction(_ sender: UIButton) {
        self.refreshButton.isEnabled = false
        self.getPresenter().fetchCurrentStateForUserLocation()
    }
}
