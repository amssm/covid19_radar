//
//  StateSituationTrafficPresenter.swift
//  COVID19Radar
//
//  Created by Ahmad Mahmoud on 22/12/2020.
//

import RxSwift
import iOS_Bootstrap

class StateSituationTrafficPresenter: AppPresenter<StateSituationTrafficView>, LocationManagerDelegate {
    
    private var repo: StateSituationTrafficRepo!
    private var locationManager: LocationManager!
    private var stateInfoDisposable: Disposable?

    required convenience init(viewDelegate: StateSituationTrafficView,
                              stateSituationTrafficRepo: StateSituationTrafficRepo,
                              locationManager: LocationManager) {
        self.init(viewDelegate: viewDelegate)
        self.repo = stateSituationTrafficRepo
        self.locationManager = locationManager
        self.locationManager.setDelegate(self)
    }
    
    override func viewControllerDidLoad() {
        self.fetchCurrentStateForUserLocation()
    }
    
    func fetchCurrentStateForUserLocation() {
        // Fetch user location and in the location callback, I'm fetching state info.
        self.locationManager.getCurrentLocationCoordiantes()
    }
    
    func didGetLocationCoordinates(lat: Double, longt: Double) {
        self.fetchBavariaStateInfo()
    }
    
    func didFailToGetLocationCoordinates() {
        self.getViewDelegate().didFailtToGetLocationCoordinates()
    }
    
    func didFailToGetLocationCoordinates(with error: Error) {
        self.getViewDelegate().showError(errorMessage: error.localizedDescription)
    }
    
    func didDenyLocationPermission() {
        self.getViewDelegate().didDenyLocationPermission()
    }
    
    private func fetchBavariaStateInfo() {
        self.getViewDelegate().showLoading()
        // 10 mins. == 600 seconds
        let repeatInterval = RxTimeInterval.seconds(600)
        self.stateInfoDisposable?.cancelRequest()
        self.stateInfoRequest()
            .ignoreErrors()
            .subscribe()
            .disposed(by: disposeBag)
        self.stateInfoDisposable = Observable<Int>
            // Rebeat every 10 mins.
            .interval(repeatInterval, scheduler: RxSchedulers.main)
            .flatMap({ [weak self] _ -> Observable<StateInfo> in
                let error = AppError(message: "something_went_wrong".localizedString())
                let result = self?.stateInfoRequest()
                return (result ?? Observable.error(error))
            })
            .subscribe()
    }
    
    private func stateInfoRequest() -> Observable<StateInfo> {
        return self.repo
            .getBavariaStateInfo()
            .asObservable()
            // Simulate netword delay
            .delay(RxTimeInterval.seconds(3), scheduler: RxSchedulers.main)
            .applyThreadingConfig()
            // Emit result only if the result is different from the previous one
            .distinctUntilChanged({ (oldState, newState) -> Bool in oldState == newState })
            .do(onNext:  { [weak self] info in
                guard let strongSelf = self else { return }
                strongSelf.getViewDelegate().hideLoading()
                strongSelf.processStateInfo(info: info)
            }, onError: { [weak self] error in
                guard let strongSelf = self else { return }
                strongSelf.postError(errorMessage: error.localizedDescription)
            })
    }
    
    private func processStateInfo(info: StateInfo) {
        var state: State
        // Green state
        if(info.casesPer100k < 35) {
            state = State(color: .Green, precautions: "green_state".localizedString())
        }
        else if(35...50 ~= info.casesPer100k) {
            state = State(color: .Yellow, precautions: "yellow_state".localizedString())
        }
        else if(50...100 ~= info.casesPer100k) {
            state = State(color: .Red, precautions: "red_state".localizedString())
        }
        else {
            state = State(color: .DarkRed, precautions: "dark_red_state".localizedString())
        }
        self.getViewDelegate().didGetCurrentState(state: state)
    }
    
    deinit {
        self.stateInfoDisposable?.dispose()
    }
}
