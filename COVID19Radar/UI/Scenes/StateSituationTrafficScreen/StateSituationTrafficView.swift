//
//  StateSituationTrafficView.swift
//  COVID19Radar
//
//  Created by Ahmad Mahmoud on 23/12/2020.
//

protocol StateSituationTrafficView: AppViewDelegate {
    func didDenyLocationPermission()
    func didFailtToGetLocationCoordinates()
    func didGetCurrentState(state: State)
}
