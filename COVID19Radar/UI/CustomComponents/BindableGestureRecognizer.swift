//
//  BindableGestureRecognizer.swift
//  COVID19Radar
//
//  Created by Ahmad Mahmoud on 25/12/2020.
//

import UIKit

final class BindableGestureRecognizer: UITapGestureRecognizer {
    
    private var action: () -> Void

    init(action: @escaping () -> Void) {
        self.action = action
        super.init(target: nil, action: nil)
        self.addTarget(self, action: #selector(execute))
    }

    @objc private func execute() {
        action()
    }
}
