//
//  TrafficLight.swift
//  COVID19Radar
//
//  Created by Ahmad Mahmoud on 22/12/2020.
//

import UIKit
import iOS_Bootstrap
import RxSwift

@IBDesignable
class TrafficLight: UIView {
    
    @IBOutlet private var view: UIView!
    @IBOutlet private weak var stackView: UIStackView!
    @IBOutlet private weak var greenLamp: UIView!
    @IBOutlet private weak var yellowLamp: UIView!
    @IBOutlet private weak var redLamp: UIView!
    @IBOutlet private weak var darkRedLamp: UIView!
    //
    private var flashingTimer: Timer?
    private var subscription: Disposable?

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.loadViewFromNib()
        self.initUI()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadViewFromNib()
        self.initUI()
    }
    
    private func loadViewFromNib() {
        let bundle = Bundle.init(for: self.classForCoder)
        bundle.loadNibNamed(TrafficLight.nibName, owner: self, options: nil)
        self.view.frame = bounds
        self.addSubview(self.view)
    }
    
    private func initUI() {
        self.view.backgroundColor = UIColor.lightBLue
        self.view.layer.cornerRadius = 30
        self.view.layer.borderWidth = 5
        self.view.layer.borderColor = UIColor.blue.cgColor
        self.view.layer.masksToBounds = true
        self.initLamp(lamp: self.greenLamp, with: .white)
        self.initLamp(lamp: self.yellowLamp, with: .white)
        self.initLamp(lamp: self.redLamp, with: .white)
        self.initLamp(lamp: self.darkRedLamp, with: .white)
        self.stackView.subviews.forEach { $0.layer.cornerRadius = $0.frame.height / 2 }
    }
    
    private func initLamp(lamp: UIView, with color: UIColor) {
        lamp.backgroundColor = color
        lamp.layer.borderWidth = 5
        lamp.layer.borderColor = UIColor.blue.cgColor
    }
    
    private func toggleLamp(lamp: UIView) {
        lamp.backgroundColor = .white
        if(lamp != self.greenLamp) {
            self.greenLamp.backgroundColor = .green
        }
        if(lamp != self.yellowLamp) {
            self.yellowLamp.backgroundColor = .yellow
        }
        if(lamp != self.redLamp) {
            self.redLamp.backgroundColor = .red
        }
        if(lamp != self.darkRedLamp) {
            self.darkRedLamp.backgroundColor = .darkRed
        }
    }
    
    func startFlashingLights()  {
        let array = self.stackView.arrangedSubviews
        let observable = Observable
            .from(array)
            .with(interval: RxTimeInterval.milliseconds(500))
            .compactMap { $0 }
            .do(onNext: {  [weak self] lamp in
                guard let strongSelf = self else { return }
                strongSelf.toggleLamp(lamp: lamp)
            })
        subscription = observable.subscribe()
        let interval = TimeInterval(array.count/2)
        self.flashingTimer = Timer.scheduledTimer(withTimeInterval: interval, repeats: true) { [weak self] timer in
            guard let strongSelf = self else { return }
            strongSelf.subscription?.dispose()
            strongSelf.subscription = observable.subscribe()
        }
    }
    
    func stopFlasing() {
        self.subscription?.dispose()
        self.flashingTimer?.invalidate()
        self.turnOffLamp(lamp: self.greenLamp)
        self.turnOffLamp(lamp: self.yellowLamp)
        self.turnOffLamp(lamp: self.redLamp)
        self.turnOffLamp(lamp: self.darkRedLamp)
    }
    
    func turnOnLamp(stateColor: StateColor, clickAction: @escaping () -> Void) {
        let tapGesture = BindableGestureRecognizer { clickAction() }
        switch stateColor {
        case .Green:
            self.greenLamp.backgroundColor = .green
            self.greenLamp.addGestureRecognizer(tapGesture)
        case .Yellow:
            self.yellowLamp.backgroundColor = .yellow
            self.yellowLamp.addGestureRecognizer(tapGesture)
        case .Red:
            self.redLamp.backgroundColor = .red
            self.redLamp.addGestureRecognizer(tapGesture)
        case .DarkRed:
            self.darkRedLamp.backgroundColor = .darkRed
            self.darkRedLamp.addGestureRecognizer(tapGesture)
        }
    }
    
    private func turnOffLamp(lamp: UIView) {
        lamp.backgroundColor = .white
    }
}
