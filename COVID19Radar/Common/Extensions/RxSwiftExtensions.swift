//
//  RxSwiftExtensions.swift
//  COVID19Radar
//
//  Created by Ahmad Mahmoud on 22/12/2020.
//

import RxSwift
import iOS_Bootstrap

extension Observable {
    func applyThreadingConfig() -> Observable {
        return self
            .subscribe(on: RxSchedulers.backgroundConcurrentScheduler)
            .observe(on: RxSchedulers.main)
    }
    
    func with(interval: RxTimeInterval) -> Observable {
        return enumerated().concatMap { index, element in
            Observable.just(element).delay(index == 0 ? RxTimeInterval.seconds(0) : interval, scheduler: RxSchedulers.main)
            }
    }
}

extension PrimitiveSequence {
    func applyThreadingConfig() -> PrimitiveSequence {
        return self
            .subscribe(on: RxSchedulers.backgroundConcurrentScheduler)
            .observe(on: RxSchedulers.main)
    }
}
