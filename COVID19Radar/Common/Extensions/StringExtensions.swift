//
//  StringExtensions.swift
//  COVID19Radar
//
//  Created by Ahmad Mahmoud on 26/12/2020.
//

import Foundation

extension String {
    func localizedString() -> String {
        let lang = NSLocale.preferredLanguages[0].components(separatedBy: "-").first
        var path = Bundle.main.path(forResource: lang, ofType: "lproj")
        if path == nil {
            path = Bundle.main.path(forResource: "Base", ofType: "lproj")
        }
        return NSLocalizedString(self, tableName: "Localizable", bundle: Bundle(path: path!)!, value: "", comment: "")
    }
}
