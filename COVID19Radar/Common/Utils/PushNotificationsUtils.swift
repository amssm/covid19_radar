//
//  PushNotificationsUtils.swift
//  COVID19Radar
//
//  Created by Ahmad Mahmoud on 26/12/2020.
//

import UIKit
import UserNotifications

class PushNotificationsUtils: NSObject, UNUserNotificationCenterDelegate {
    
    static let shared = PushNotificationsUtils()
    //
    private let userNotificationCenter = UNUserNotificationCenter.current()

    private override init() {
        super.init()
        self.userNotificationCenter.delegate = self
    }
    
    func requestNotificationAuthorization(errorAction: @escaping (Error?) -> Void) {
        let authOptions = UNAuthorizationOptions.init(arrayLiteral: .alert, .badge, .sound)
        self.userNotificationCenter.requestAuthorization(options: authOptions) { (success, error) in
            if let error = error {
                errorAction(error)
            }
        }
    }
    
    func sendNotification() {
        self.userNotificationCenter.getNotificationSettings { (settings) in
            if settings.authorizationStatus == .authorized {
                let content = UNMutableNotificationContent()
                content.title = "state_status_update".localizedString()
                content.body = "state_status_update_message".localizedString()
                content.sound = UNNotificationSound.default
                content.badge = 1
                let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
                let identifier = "COVID19Trigger"
                let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
                self.userNotificationCenter.add(request) { (error) in
                    if let error = error {
                        print("Error \(error.localizedDescription)")
                    }
                }
            }
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        if(response.actionIdentifier == "com.apple.UNNotificationDefaultActionIdentifier") {
            let window = UIApplication.shared.windows.first
            if let navController = window?.rootViewController as? UINavigationController,
               let vc = navController.topViewController as? ViewControllerUtilsProtocol {
                let message = "click_traffic_lamp_for_more_info".localizedString()
                vc.dialogs.showDialog(viewController: vc, message: message)
            }
        }
        completionHandler()
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .badge, .sound])
    }
}
