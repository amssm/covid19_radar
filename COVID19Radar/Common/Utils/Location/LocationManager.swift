//
//  LocationManager.swift
//  COVID19Radar
//
//  Created by Ahmad Mahmoud on 23/12/2020.
//

import MapKit
import Resolver

class LocationManager: NSObject, CLLocationManagerDelegate {
    
    private let locationManager: CLLocationManager
    private var previousCoordinates: CLLocationCoordinate2D?
    private var delegate: LocationManagerDelegate?
    
    init(locationManager: CLLocationManager) {
        self.locationManager = locationManager
        super.init()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
    }
    
    func setDelegate(_ delegate: LocationManagerDelegate) {
        self.delegate = delegate
    }
            
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            self.requestLocationPermission()
            break
        case .authorizedWhenInUse:
            self.requestLocationUpdate()
            break
        case .authorizedAlways:
            self.requestLocationUpdate()
            break
        case .restricted:
            self.postLocationError()
            break
        case .denied:
            self.listenForPermissionUpdates()
            self.delegate?.didDenyLocationPermission()
            break
        default:
            self.stopLocationUpdate()
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let coordinates = locations.first?.coordinate {
            let isEqualLat = coordinates.latitude == self.previousCoordinates?.latitude
            let isEqualLongt = coordinates.longitude == previousCoordinates?.longitude
            if(!isEqualLat && !isEqualLongt) {
                self.delegate?.didGetLocationCoordinates(lat: coordinates.latitude, longt: coordinates.longitude)
                self.stopLocationUpdate()
                self.previousCoordinates = coordinates
            }
        }
    }
    
    final func getCurrentLocationCoordiantes() {
        self.previousCoordinates = nil
        if(self.hasLocationPermission()) {
            if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
                    CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways) {
                if(locationManager.location == nil) {
                    self.requestLocationUpdate()
                }
                else if let currentLocation = locationManager.location?.coordinate {
                    delegate?.didGetLocationCoordinates(lat: currentLocation.latitude, longt: currentLocation.longitude)
                    self.stopLocationUpdate()
                }
            }
        }
        else {
            self.requestLocationPermission()
        }
    }

    private final func hasLocationPermission() -> Bool {
        var hasPermission = false
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                hasPermission = false
            case .authorizedAlways, .authorizedWhenInUse:
                hasPermission = true
            @unknown default:
                hasPermission = false
            }
        }
        return hasPermission
    }

    private final func requestLocationPermission() {
        locationManager.requestWhenInUseAuthorization()
    }
    
    private final func requestLocationUpdate() { locationManager.startUpdatingLocation() }

    private final func stopLocationUpdate() { locationManager.stopUpdatingLocation() }
    
    private func postLocationError()  {
        self.stopLocationUpdate()
        self.listenForPermissionUpdates()
        self.delegate?.didFailToGetLocationCoordinates()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        self.delegate?.didFailToGetLocationCoordinates(with: error)
    }
    
    private func listenForPermissionUpdates()  {
        let name = UIApplication.willEnterForegroundNotification
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(willEnterForeground), name: name, object: nil)
    }
    
    @objc private func willEnterForeground() {
        self.getCurrentLocationCoordiantes()
    }
}
