//
//  LocationManagerDelegate.swift
//  COVID19Radar
//
//  Created by Ahmad Mahmoud on 23/12/2020.
//

protocol LocationManagerDelegate {
    func didGetLocationCoordinates(lat : Double, longt : Double)
    func didFailToGetLocationCoordinates()
    func didFailToGetLocationCoordinates(with error: Error)
    func didDenyLocationPermission()
}
