//
//  UIUtils.swift
//  COVID19Radar
//
//  Created by Ahmad Mahmoud on 25/12/2020.
//

import UIKit

struct UIUtils {
    
    static func bulletPointsFrom(stringList: [String],
                            font: UIFont = .systemFont(ofSize: 16),
                            bullet: String = "\u{2022}",
                            indentation: CGFloat = 20,
                            lineSpacing: CGFloat = 2,
                            paragraphSpacing: CGFloat = 12,
                            textColor: UIColor = .systemBlue,
                            bulletColor: UIColor = .black) -> NSAttributedString {
        let textAttributes = [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: textColor]
        let bulletAttributes = [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: bulletColor]
        let paragraphStyle = NSMutableParagraphStyle()
        let nonOptions = [NSTextTab.OptionKey: Any]()
        paragraphStyle.tabStops = [ NSTextTab(textAlignment: .left, location: indentation, options: nonOptions)]
        paragraphStyle.defaultTabInterval = indentation
        paragraphStyle.lineSpacing = lineSpacing
        paragraphStyle.paragraphSpacing = paragraphSpacing
        paragraphStyle.headIndent = indentation
        let bulletList = NSMutableAttributedString()
        for string in stringList {
            let formattedString = "\(bullet)\t\(string)\n"
            let attributedString = NSMutableAttributedString(string: formattedString)
            
            attributedString.addAttributes(
                [NSAttributedString.Key.paragraphStyle : paragraphStyle],
                range: NSMakeRange(0, attributedString.length))
            
            attributedString.addAttributes(
                textAttributes,
                range: NSMakeRange(0, attributedString.length))
            
            let string:NSString = NSString(string: formattedString)
            let rangeForBullet:NSRange = string.range(of: bullet)
            attributedString.addAttributes(bulletAttributes, range: rangeForBullet)
            bulletList.append(attributedString)
        }
        return bulletList
    }
}
