//
//  Colors.swift
//  COVID19Radar
//
//  Created by Ahmad Mahmoud on 22/12/2020.
//

import UIKit

extension UIColor {
    static let lightBLue = UIColor(red: 173, green: 207, blue: 230)
    static let darkRed = UIColor(red: 139, green: 0, blue: 0)
}
