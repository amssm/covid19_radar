//
//  AppDelegate+Injection.swift
//  COVID19Radar
//
//  Created by Ahmad Mahmoud on 22/12/2020.
//

import Resolver

extension Resolver: ResolverRegistering {
    public static func registerAllServices() {
        Resolver.defaultScope = Resolver.unique
        presenterModules()
        restModules()
        repositoriesModules()
        otherModules()
    }
}
