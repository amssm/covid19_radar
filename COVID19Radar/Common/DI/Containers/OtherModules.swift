//
//  OtherModules.swift
//  COVID19Radar
//
//  Created by Ahmad Mahmoud on 22/12/2020.
//

import MapKit
import Resolver

extension Resolver {
    static func otherModules() {
        register { Dialogs() }
        register { Navigator() }.scope(application)
        register { CLLocationManager() }
        register { LocationManager(locationManager: resolve()) }
    }
}
