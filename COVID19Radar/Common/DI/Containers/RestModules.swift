//
//  RestModules.swift
//  COVID19Radar
//
//  Created by Ahmad Mahmoud on 22/12/2020.
//

import RxSwift
import Resolver
import iOS_Bootstrap

extension Resolver {
    
    static func restModules() {
        register { AppRestClient() }.implements(RxAlamofireClientProtocol.self).scope(application)
        // APIs
        register { GermnaStatesWithInfoAPI() }
        register { (_, _) -> Single<GermanStatesResponse> in
            return getRxAPI(api: GermnaStatesWithInfoAPI())
        }
    }
}

