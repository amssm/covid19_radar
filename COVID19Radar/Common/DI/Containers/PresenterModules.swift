//
//  PresenterModules.swift
//  COVID19Radar
//
//  Created by Ahmad Mahmoud on 22/12/2020.
//

import Resolver

extension Resolver {
    static func presenterModules() {
        register { (resolver, args) -> StateSituationTrafficPresenter in
            let viewDelegate: StateSituationTrafficView = resolver.arg0(from: args!)!
            return StateSituationTrafficPresenter(viewDelegate: viewDelegate,
                                                  stateSituationTrafficRepo: resolve(),
                                                  locationManager: resolve())
        }
    }
}
