//
//  RepositoriesModules.swift
//  COVID19Radar
//
//  Created by Ahmad Mahmoud on 22/12/2020.
//

import Resolver

extension Resolver {
    static func repositoriesModules() {
        register { StateSituationTrafficRepo(germanStatesAPI: resolve()) }
    }
}
