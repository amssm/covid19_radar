//
//  Resolver+Utils.swift
//  COVID19Radar
//
//  Created by Ahmad Mahmoud on 22/12/2020.
//

import RxSwift
import Resolver
import Alamofire
import iOS_Bootstrap

extension Resolver {
    
    private static func resolveRestClient() -> RxAlamofireClientProtocol {
        return Resolver.resolve(RxAlamofireClientProtocol.self)
    }
    
    static func getRxAPI<T: Decodable, API: AlamofireAPI>(api: API) -> Single<T> {
        return resolveRestClient().request(api: api)
    }
}
