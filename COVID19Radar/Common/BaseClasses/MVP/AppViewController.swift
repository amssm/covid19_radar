//
//  AppViewController.swift
//  COVID19Radar
//
//  Created by Ahmad Mahmoud on 22/12/2020.
//

import Resolver
import iOS_Bootstrap

class AppViewController<P, V>: BaseViewController<P, V>,
                               ViewControllerUtilsProtocol,
                               Resolving
                               where P: BasePresenter<V>  {
    
    @LazyInjected var dialogs: Dialogs
    @LazyInjected var navigator: Navigator

    override func initUI() {}
    
    override func localizeStrings() {}
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    final func removeNavigationBarBottomBorder() {
        navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    final func restoreNavigationBarBottomBorder() {
        navigationController?.navigationBar.shadowImage = nil
    }
    
    func setNavigationBarTitle(title: String) { self.navigationItem.title = title }
    
    func networkConnectionMonitoringEnabled() -> Bool { return true }
    
    override func didGetError(errorMessage: String) {
        showError(errorMessage: errorMessage)
    }
    
    override func didGetWarning(warningMessage: String) {
        showWarning(warningMessage: warningMessage)
    }
    
    override func showLoading() {
        dialogs.showLoading(viewController: self)
    }
    
    override func hideLoading() {
        dialogs.hideDialog()
    }
}
