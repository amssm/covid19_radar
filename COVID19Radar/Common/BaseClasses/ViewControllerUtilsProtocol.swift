//
//  ViewControllerUtilsProtocol.swift
//  COVID19Radar
//
//  Created by Ahmad Mahmoud on 26/12/2020.
//

import UIKit.UIViewController

protocol ViewControllerUtilsProtocol where Self: UIViewController {
    var dialogs: Dialogs { get }
    var navigator: Navigator { get }
}
