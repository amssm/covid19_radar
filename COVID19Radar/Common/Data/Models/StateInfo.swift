//
//  StateInfo.swift
//  COVID19Radar
//
//  Created by Ahmad Mahmoud on 24/12/2020.
//

struct StateInfo: Decodable, Equatable {
    let name: String
    let code: String
    let count: Int
    let weekIncidence: Double
    let casesPer100k: Double
    let deaths: Int64
}
