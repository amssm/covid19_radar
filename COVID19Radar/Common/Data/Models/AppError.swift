//
//  AppError.swift
//  COVID19Radar
//
//  Created by Ahmad Mahmoud on 25/12/2020.
//

import Foundation

struct AppError: LocalizedError {
   
    private let message: String
    var errorDescription: String? { return message }
    
    init(message: String) {
        self.message = message
    }
}
