//
//  GermanStatesResponse.swift
//  COVID19Radar
//
//  Created by Ahmad Mahmoud on 24/12/2020.
//

struct GermanStatesResponse: Decodable {
    let lastUpdate: Int64
    let states: [StateInfo]
}
