//
//  StateColor.swift
//  COVID19Radar
//
//  Created by Ahmad Mahmoud on 24/12/2020.
//

enum StateColor {
    case Green
    case Yellow
    case Red
    case DarkRed
}
