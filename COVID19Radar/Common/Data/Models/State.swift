//
//  State.swift
//  COVID19Radar
//
//  Created by Ahmad Mahmoud on 24/12/2020.
//

struct State {
    
    let color: StateColor
    let precautions: String
    
    init(color: StateColor, precautions: String) {
        self.color = color
        self.precautions = precautions
    }
    
    func getPrecautions() -> [String] {
        precautions.components(separatedBy: "\n")
    }
}
