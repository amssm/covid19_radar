//
//  Endpoints.swift
//  COVID19Radar
//
//  Created by Ahmad Mahmoud on 22/12/2020.
//

// Enums cannot be instantiated that's why I used it in this usecase
enum Endpoints {
    static let germanStates = "/states/"
}
