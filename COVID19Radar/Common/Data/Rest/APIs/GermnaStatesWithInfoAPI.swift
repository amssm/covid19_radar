//
//  GermnaStatesWithInfoAPI.swift
//  COVID19Radar
//
//  Created by Ahmad Mahmoud on 22/12/2020.
//

import iOS_Bootstrap

class GermnaStatesWithInfoAPI: AppAPI {
    var route: Route { return .get(Endpoints.germanStates) }
}
