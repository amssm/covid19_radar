//
//  AppRestClient.swift
//  COVID19Radar
//
//  Created by Ahmad Mahmoud on 22/12/2020.
//

import RxSwift
import iOS_Bootstrap

class AppRestClient: RxAlamofireRestClient {
    
    override func getBaseURL() -> String { "https://rki.marlon-lueckert.de/api" }
    
    override func shouldEnableConsoleLogging() -> Bool { true }
}
