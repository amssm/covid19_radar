//
//  AppDelegate.swift
//  COVID19Radar
//
//  Created by Ahmad Mahmoud on 22/12/2020.
//

import UIKit
import Resolver
import iOS_Bootstrap

class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    private var defaultConfigs: DefaultConfigurations?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.startInitialViewController()
        return true
    }
    
    private func startInitialViewController() {
        let navigator: Navigator = Resolver.resolve()
        navigator.startInitialViewController()
    }
}
