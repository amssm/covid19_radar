//
//  Navigator.swift
//  COVID19Radar
//
//  Created by Ahmad Mahmoud on 22/12/2020.
//

import UIKit

class Navigator {
        
    private lazy var window: UIWindow = {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.window!
    }()
    
    func startInitialViewController() {
        let mainViewController = StateSituationTrafficViewController()
        let navController = UINavigationController(rootViewController: mainViewController)
        //
        UIView.transition(
            with: window,
            duration: 0.3,
            options: .transitionFlipFromLeft,
            animations: {
                self.window.rootViewController = navController
                self.window.makeKeyAndVisible()
        }, completion: nil)
    }
    
    func openLocationSettings() {
        if let url = URL(string: UIApplication.openSettingsURLString) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    func openPrecautionsScreen(from viewController: UIViewController, precautions: [String]) {
        let destinationViewCOntroller = PrecautionsViewController(precautions: precautions)
        viewController.navigationController?.pushViewController(destinationViewCOntroller, animated: true)
    }
}
