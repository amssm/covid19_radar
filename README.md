# COVID19_RADAR

# Intro:
- When you decompress the attached file, you will find two folders, one for Android and one for iOS, each folder also contains two mobile projects (Four in total, Two for each mobile platform), the project named **FrontEndChallenge**, has the solutions for the second and the third questions in the PDF (Anagrams and Fibonacci number), and the project named **CurrencyConverter** (Also one for Android and one for iOS) is the currency converter sample app.

## Requirements:
- [x]  Use GIT.
- [x] follow the DDD Pattern, use reusable components and Data Sources where ever possible.
- [x] make your Code as SOLID as possible.
- [x] work with the cleanCode principles in mind.
- [x] make your UI Work in Portrait style, you do not have to make it work in landscape.
- [ ] use SWIFT 5.3 to develop this app, use the swift package manager when you think it make sense.
- [x] use localized Strings where ever text is displayed (NSLocalizedString(...)).
- [x] Text in the app should be displayed in German (if the system language is german - or in english - if the system language is not German).
- [x] The application should check every 10 minutes for the property cases7_per_100k from the official rest api for the actual coordinates..




### Notes about the task:

1. I used **VIPER** as the design pattern of the task, where the **Repo.** is the **Interactor**.

2. I used **Navigator** to handle the navigation between view controllers, **Navigator** is the **Router** layer of **VIPER** pattern which is responsible for the navigation from one view to another.

2. I used [iOS Bootstrap](https://github.com/ahmadmssm/iOS_Bootstrap), which is my open-source iOS library, It is a wrapper around Alamofire plus some useful utilities that facilitate the development process, I also used **RxSwift** (Pods and not Swift package manager).

3. I also used [Resolver](https://github.com/hmlongco/Resolver) as a dependency injection framework, I'm also contributing to this open-source library.

4. I used this [API](https://rki.marlon-lueckert.de/) to fetch German state **Bavaria** info, I also designed custom traffic components and used Rx to schedule the updates every 10 mins.

5. I have also added **GPX** to simulate **Bavaria** so kindly allow location simulation and set the simulated location to this file.

6. I wanted to include unit tests for both platforms but the time is very tight for me as I'm working on many projects and actually one of them aims to help with the COVID-19 situation.

Thank you very much...
